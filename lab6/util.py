from matplotlib.axes import Axes


def disable_axes_ticks(ax: Axes):
    ax.tick_params(
        axis="both",
        which="both",
        bottom=False,
        top=False,
        left=False,
        right=False,
        labelleft=False,
        labelbottom=False,
    )
