#!/usr/bin/env bash
set -e

function nth_dirname {
    local dir=${1}
    local n=${2:-1}
    for i in $(seq 1 $n)
    do
        dir=$(dirname $dir)
    done
    echo $dir
}

PROJECT_DIR=$(nth_dirname $(readlink -e "${BASH_SOURCE[0]}") 2)

NOTEBOOK_REGEX="${PROJECT_DIR}/lab[0-9]+/[^/]+.ipynb"
find $PROJECT_DIR -regex $NOTEBOOK_REGEX | parallel jupytext --sync {}
