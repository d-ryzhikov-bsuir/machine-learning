from typing import Generator

import numpy as np


def f1_score(y: np.ndarray, y_val: np.ndarray) -> float:
    """F1 score function.

    F1 = 2 * Precision * Recall / (Precision + Recall),
    where
        Precision =  True Positive / (True Positive + False Positive)
        Recall = True Positive / (True Positive + False Positive)
    """

    assert y.shape == y_val.shape

    true_positive = np.sum(np.logical_and(y == 1, y_val == 1))
    false_positive = np.sum(np.logical_and(y == 1, y_val == 0))
    false_negative = np.sum(np.logical_and(y == 0, y_val == 1))

    true_positive_false_positive = true_positive + false_positive
    true_positive_false_negative = true_positive + false_negative

    if true_positive_false_positive == 0 or true_positive_false_negative == 0:
        return 0

    precision = true_positive / true_positive_false_positive
    recall = true_positive / true_positive_false_negative

    return 2 * precision * recall / (precision + recall)


def threshold_gen(start=0.1) -> Generator[float, None, None]:
    while True:
        yield start
        yield start / 2
        start /= 10
