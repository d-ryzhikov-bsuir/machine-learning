# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:sphinx
#     text_representation:
#       extension: .py
#       format_name: sphinx
#       format_version: '1.1'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %load_ext autoreload
# %autoreload 2

"""
# Лабораторная работа №8, часть 2
"""

from itertools import islice

import numpy as np
from matplotlib import pyplot as plt
from pandas import DataFrame
from scipy.io import loadmat
from scipy.stats import multivariate_normal

from common import f1_score, threshold_gen

###############################################################################
# ## Загрузка данных

DATA = loadmat("ex8data2.mat")

X_TRAIN = DATA["X"]
EXAMPLE_COUNT = X_TRAIN.shape[0]
DIMENSION_COUNT = X_TRAIN.shape[1]

X_VAL = DATA["Xval"]
Y_VAL = DATA["yval"]
Y_VAL_FLATTENED = np.ravel(Y_VAL)

###############################################################################
# ### Гистограммы случайных величин

GRAPH_COLS = 4
GRAPH_ROWS = -(-DIMENSION_COUNT // GRAPH_COLS)  # ceil division

hist_fig, hist_axes = plt.subplots(ncols=GRAPH_COLS, nrows=GRAPH_ROWS, figsize=(16, 10))
hist_axes = hist_axes.flatten()

empty_axes_count = GRAPH_ROWS * GRAPH_COLS - DIMENSION_COUNT
for ax in hist_axes[-empty_axes_count:]:
    ax.axis("off")

for index in range(DIMENSION_COUNT):
    ax = hist_axes[index]
    ax.hist(X_TRAIN[:, index], bins=100)
    ax.set_title(f"X{index + 1}")

plt.show()

###############################################################################
# ### Параметры распределения случайных величин

means = np.mean(X_TRAIN, axis=0)
variances = np.var(X_TRAIN, axis=0)
scales = np.sqrt(variances)

print(f"Means = {means}")
print(f"Variances = {variances}")
print(f"Scales = {scales}")

###############################################################################
# #### Ковариационная матрица
# \begin{equation}
# \Sigma = \frac{1}{m - 1} \sum_{i=1}^{m}
# \left(
# X_i - \mu
# \right)^T
# \left(
# X_i - \mu
# \right)
# \end{equation}

deviations = X_TRAIN - means
covariance_matrix = (deviations.T @ deviations) / (EXAMPLE_COUNT - 1)
DataFrame(covariance_matrix)

###############################################################################
# ### Многомерное нормальное распределение
# \begin{equation}
# p(x; \mu, \Sigma) = \frac{1}{(2 \pi)^{\frac{n}{2}}\det \Sigma ^{\frac{1}{2}}}
# \exp
# \left(
# -\frac{1}{2}(x - \mu)^T \Sigma^{-1}(x - \mu)
# \right)
# \end{equation}

configured_multivariate_normal = multivariate_normal(mean=means, cov=covariance_matrix)

###############################################################################
# ### Получение вероятностей для примеров

probabilities_val = configured_multivariate_normal.pdf(X_VAL)

###############################################################################
# ### Подбор значения порога для обнаружения аномалий

OPTIMAL_THRESHOLD = None
best_score = 0
for threshold in islice(threshold_gen(1e-18), 8):
    anomaly_hypotheses = probabilities_val < threshold
    score = f1_score(anomaly_hypotheses, Y_VAL_FLATTENED)
    if score > best_score:
        best_score = score
        OPTIMAL_THRESHOLD = threshold
    print(f"threshold = {threshold:6.2}\t score = {score:.2%}")

print("=" * 69)
print(f"Optimal threshold = {OPTIMAL_THRESHOLD:.2}")

###############################################################################
# ### Поиск аномалий в обучающей выборке

probabilities_train = configured_multivariate_normal.pdf(X_TRAIN)
anomalies_train = X_TRAIN[probabilities_train < OPTIMAL_THRESHOLD]
anomalies_count = anomalies_train.shape[0]
print(f"{anomalies_count} anomal{'y' if anomalies_count == 1 else 'ies'} found.")
