# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:sphinx
#     text_representation:
#       extension: .py
#       format_name: sphinx
#       format_version: '1.1'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %load_ext autoreload
# %autoreload 2

"""
# Лабораторная работа №2, часть 2
"""

from functools import partial, reduce
from itertools import chain, combinations_with_replacement, islice
from operator import mul
from typing import Iterable, NamedTuple, Optional, TypeVar

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.lines import Line2D
from more_itertools import last
from nptyping import Array
from scipy.optimize import OptimizeResult, minimize
from scipy.special import comb, expit, xlog1py

from common import (
    cost,
    gradient_descent_regularized,
    hypothesis,
    learning_rate_gen,
    loss_convergence_display,
    loss_regularized,
)

###############################################################################
# ## Загрузка данных
TEST_1_COLUMN_NAME = "test_1"
TEST_2_COLUMN_NAME = "test_2"
CONTROL_PASSED_COLUMN_NAME = "control_passed"

FEATURE_COLUMN_NAMES = (TEST_1_COLUMN_NAME, TEST_2_COLUMN_NAME)
FEATURE_COUNT = len(FEATURE_COLUMN_NAMES)

ParamType = np.single
ParamVector = Array[ParamType, 0, ...]
ParamMatrix = Array[ParamType, ..., ...]

DATA = pd.read_csv(
    "ex2data2.txt",
    header=None,
    names=[TEST_1_COLUMN_NAME, TEST_2_COLUMN_NAME, CONTROL_PASSED_COLUMN_NAME],
    dtype=ParamType,
)
EXAMPLE_COUNT = DATA.shape[0]

FEATURES = DATA[[TEST_1_COLUMN_NAME, TEST_2_COLUMN_NAME]].to_numpy()
TARGETS = DATA[CONTROL_PASSED_COLUMN_NAME].to_numpy()


###############################################################################
# ## Отображение входных данных
class ElementMarker(NamedTuple):
    marker: str
    color: str
    label: str


passed_marker = ElementMarker(marker="o", color="blue", label="Passed")
did_not_pass_marker = ElementMarker(marker="x", color="red", label="Did not pass")

legend_elements = [
    Line2D(
        [0],
        [0],
        marker=element_marker.marker,
        color="w",
        markerfacecolor=element_marker.color,
        markeredgecolor=element_marker.color,
        label=element_marker.label,
    )
    for element_marker in (passed_marker, did_not_pass_marker)
]

data_fig, data_axes = plt.subplots()

for row in DATA.itertuples():
    element_marker = passed_marker if row.control_passed else did_not_pass_marker
    data_axes.scatter(
        row.test_1, row.test_2, marker=element_marker.marker, c=element_marker.color
    )

data_axes.set_xlabel("Test 1")
data_axes.set_ylabel("Test 2")
data_axes.legend(handles=legend_elements, loc="upper right")
plt.show()

###############################################################################
# ## Комбинации признаков
FeatureType = TypeVar("FeatureType")


def feature_combinations(
    features: Iterable[FeatureType], degree: int
) -> Iterable[FeatureType]:
    """
    Sum up polynomial with features up to the degree.
    """

    return chain.from_iterable(
        (reduce(mul, comb, 1) for comb in combinations_with_replacement(features, i))
        for i in range(degree + 1)
    )


def get_number_of_coefficients(n_features: int, degree: int) -> int:
    return sum(comb(2, i, repetition=True, exact=True) for i in range(degree + 1))


print(f"Initial feature set size: {FEATURE_COUNT}")

POLYNOMIAL_DEGREE = 6
print(f"Maximum polynomial degree: {POLYNOMIAL_DEGREE}")

EXTENDED_FEATURE_COUNT = get_number_of_coefficients(
    len(FEATURE_COLUMN_NAMES), POLYNOMIAL_DEGREE
)
print(f"Size of extended feature set: {EXTENDED_FEATURE_COUNT}")


###############################################################################
# ### Расширенный набор признаков
def extend_feature_set(features: ParamMatrix, degree: int) -> ParamMatrix:
    extended_features = np.fromiter(
        chain.from_iterable(feature_combinations(row, degree) for row in features),
        dtype=ParamType,
    )
    example_count, feature_count = features.shape
    extended_features.shape = (
        example_count,
        get_number_of_coefficients(feature_count, degree),
    )
    return extended_features


EXTENDED_FEATURES = extend_feature_set(FEATURES, POLYNOMIAL_DEGREE)
print(EXTENDED_FEATURES[:3])


###############################################################################
# ### Представление гипотезы
# $$
# \begin{equation}
# \large
# h_{\theta}(x) = g(\theta^{T}x)
# \end{equation}
# $$
#
# $$
# \begin{equation}
# g(z) = \dfrac{1}{1 + e^{-z}}
# \end{equation}
# $$
??hypothesis
assert np.array_equal(
    np.ones(3), hypothesis(np.repeat(float("inf"), 3), np.ones((3, 3)))
)

###############################################################################
# ### Функция стоимости для элемента
# $$
# \begin{align*}
# \mathrm{Cost}(h_{\theta}(x), y) = -\log(h_{\theta}(x)) \; \text{ где y = 1 }\\
# \mathrm{Cost}(h_{\theta}(x), y) = -\log(1 - h_{\theta}(x)) \; \text{ где y = 0 }
# \end{align*}
# $$
#
# или
#
# $$
# \begin{align*}
# \normalsize
# \mathrm{Cost}(h_{\theta}(x), y) =
# -y\log(h_{\theta}(x)) - (1 - y) \log(1 - h_{\theta}(x))
# \end{align*}
# $$
??cost
assert np.array_equal(np.zeros(3), cost(np.ones(3), np.ones(3)))

###############################################################################
# ### Полная регуляризованная функция стоимости
# $$
# \begin{equation}
# \large
# J(\theta) = \dfrac{1}{m} \sum_{i=0}^{m} \mathrm{Cost}(h_{\theta}(x^{(i)}, y^{(i)}) =\\
# = -\dfrac{1}{m} \sum_{i=0}^{m}
# \left[
# y^{(i)}\log(h_{\theta}(x^{(i)}))
# + (1 - y^{(i)}) \log(1 - h_{\theta}(x^{(i)}))
# + \dfrac{\lambda}{2m} \sum_{j=1}^{n} \theta^{2}_{j=1}
# \right]
# \end{equation}
# $$

??loss_regularized

###############################################################################
# ### Грандиентный спуск
# $$
# \begin{equation}
# \large
# \theta_j := \theta_j - \frac{\alpha}{m}
# \left[
# \sum_{i=1}^m (h_\theta(x^{(i)}) - y^{(i)}) x_j^{(i)}
# + \lambda \theta_j
# \right]
# \end{equation}
# $$
# ??gradient_descent_regularized

assert np.allclose(
    np.repeat(1e10, 3),
    next(
        gradient_descent_regularized(
            1, 0, np.repeat(1e10, 3), np.ones((3, 3)), np.ones(3)
        )
    ),
)


###############################################################################
# ## Поиск оптимального значения скорости обучения
THETA_START = np.zeros(EXTENDED_FEATURE_COUNT)

loss_for_theta = partial(loss_regularized, x=EXTENDED_FEATURES, y=TARGETS, reg_param=0)
gradient_descent_for_learning_rate = partial(
    gradient_descent_regularized,
    reg_param=0,
    theta=THETA_START,
    x=EXTENDED_FEATURES,
    y=TARGETS,
)

learning_rate_fig, learning_rate_axes = plt.subplots()
for rate in islice(learning_rate_gen(1), 5):
    loss_convergence_display(
        learning_rate_axes,
        rate,
        thetas=gradient_descent_for_learning_rate(rate),
        loss_for_theta=loss_for_theta,
        max_iterations=250,
    )

# Оптимальное значение
LEARNING_RATE = ParamType(1)

plt.legend()
plt.show()


###############################################################################
# ## Поиск решения
REG_PARAM = ParamType("0.2")

iterations, gradient_theta = last(
    enumerate(
        islice(
            gradient_descent_regularized(
                LEARNING_RATE, REG_PARAM, THETA_START, x=EXTENDED_FEATURES, y=TARGETS
            ),
            100000,
        ),
        1,
    )
)
print(f"iterations = {iterations}")
print(f"theta = {gradient_theta}")
print(
    f"loss = {loss_regularized(gradient_theta, EXTENDED_FEATURES, TARGETS, REG_PARAM)}"
)


###############################################################################
# ### Построение разделяющей прямой
def legend_handle(color: Optional[str] = None, label: Optional[str] = None) -> Line2D:
    return Line2D([0], [0], color=color, label=label)


def display_decision_boundary(
    ax: Axes,
    grid_steps: int,
    theta: ParamVector,
    x1_values: ParamVector,
    x2_values: ParamVector,
    color=None,
):
    hypotheses = np.empty((grid_steps, grid_steps), dtype=ParamType)
    for i, x1 in enumerate(x1_values):
        for j, x2 in enumerate(x2_values):
            hypotheses[i, j] = theta @ np.fromiter(
                feature_combinations((x1, x2), POLYNOMIAL_DEGREE), dtype=ParamType
            )
    ax.contour(x1_values, x2_values, hypotheses, 0, colors=color)


GRID_STEPS = 100
VALUES_OFFSET = ParamType("0.3")

x1_min = min(FEATURES[:, 0])
x1_max = max(FEATURES[:, 0])
x1_values = np.linspace(x1_min - VALUES_OFFSET, x1_max + VALUES_OFFSET, num=GRID_STEPS)

x2_min = min(FEATURES[:, 1])
x2_max = max(FEATURES[:, 1])
x2_values = np.linspace(x2_min - VALUES_OFFSET, x2_max + VALUES_OFFSET, num=GRID_STEPS)

decision_boundary_for_theta = partial(
    display_decision_boundary,
    grid_steps=GRID_STEPS,
    x1_values=x1_values,
    x2_values=x2_values,
)

###############################################################################
# ### Разделяющая прямая для регуляризованного градиентного спуска
color = "red"
label = "Gradient Descent"
decision_boundary_for_theta(ax=data_axes, theta=gradient_theta, color=color)
data_axes.legend(handles=[legend_handle(color, label)])
data_fig


###############################################################################
# ### Функиця предсказания
def predict(theta: ParamVector, x: ParamMatrix) -> ParamMatrix:
    return (x @ theta) >= ParamType("0.5")


def get_score(predictions: ParamVector, targets: ParamVector) -> float:
    length = len(predictions)
    assert length == len(targets)
    return sum(predictions == targets) / length


gradient_score = get_score(
    predict(gradient_theta, EXTENDED_FEATURES), TARGETS.astype(np.bool)
)
print(f"Gradient descent score = {gradient_score:.1%}")


###############################################################################
# ### Отображение результатов оптимизации
def display_optimization(optimize_result: OptimizeResult, ax: Axes, color=None):
    print(f"iterations = {optimize_result.nit}")
    print(f"theta = {optimize_result.x}")
    print(f"loss = {optimize_result.fun}")
    score = get_score(predict(optimize_result.x, EXTENDED_FEATURES), TARGETS)
    print(f"score = {score:.1%}")
    decision_boundary_for_theta(ax=ax, theta=optimize_result.x, color=color)


###############################################################################
# ## Алгоритм Бройдена — Флетчера — Гольдфарба — Шанно с ограниченной памятью
color = "blue"
label = "L-BFGS"
l_bfgs_optimization = minimize(
    fun=loss_regularized,
    x0=THETA_START,
    args=(EXTENDED_FEATURES, TARGETS, REG_PARAM),
    method="BFGS",
    tol=ParamType("1e-6"),
)
display_optimization(l_bfgs_optimization, data_axes, color=color)
data_axes.legend(handles=[legend_handle(color, label)])
data_fig


###############################################################################
# ## Влияние регуляризационного параметра на результаты
class RegParamColor(NamedTuple):
    value: ParamType
    color: str


params = (
    RegParamColor(0, "green"),
    RegParamColor(ParamType("0.2"), "orange"),
    RegParamColor(ParamType("10"), "purple"),
)
for reg_param, color in params:
    print(f"Regularization parameter: {reg_param:.2f}")
    optimization = minimize(
        fun=loss_regularized,
        x0=THETA_START,
        args=(EXTENDED_FEATURES, TARGETS, reg_param),
        method="BFGS",
        tol=ParamType("1e-6"),
    )
    display_optimization(optimization, data_axes, color=color)
    print("=" * 10)

data_axes.legend(
    handles=[legend_handle(p.color, str(p.value)) for p in params], loc="upper right"
)
data_fig
