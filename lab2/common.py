from itertools import islice
from typing import Callable, Iterable

import numpy as np
from matplotlib.axes import Axes
from nptyping import Array
from scipy.special import expit, xlog1py


def learning_rate_gen(start=1):
    """Learning rate generator.

    >>> from itertools import islice
    >>> for rate in islice(learning_rate_gen(), 3):
    ...     print(f'{rate:.1f}')
    1.0
    0.3
    0.1
    """
    current_rate = start
    while True:
        yield current_rate
        current_rate /= 10
        yield current_rate * 3


ParamType = np.float

ParamVector = Array[ParamType, ...]
ParamMatrix = Array[ParamType, ..., ...]


def loss_convergence_display(
    ax: Axes,
    learning_rate: ParamType,
    thetas: Iterable[ParamVector],
    loss_for_theta: Callable[[ParamVector], ParamType],
    max_iterations: int = 100,
):
    loss_values = [loss_for_theta(t) for t in islice(thetas, max_iterations)]
    ax.plot(
        list(range(1, len(loss_values) + 1)),
        loss_values,
        label=str(learning_rate)
    )


def sigmoid(theta: ParamVector, x: ParamMatrix) -> ParamVector:
    return expit(x @ theta)


def hypothesis(theta: ParamVector, x: ParamMatrix) -> ParamVector:
    return sigmoid(theta=theta, x=x)


def cost(h: ParamVector, y: ParamVector) -> ParamType:
    """Element-wise cost function."""
    assert h.shape == y.shape

    return np.multiply(-y, np.log(h)) - xlog1py(1 - y, -h)


def loss_regularized(
    theta: ParamVector, x: ParamMatrix, y: ParamVector, reg_param: ParamType
) -> ParamType:
    """Total cost function."""
    length = y.shape[0]
    assert length == x.shape[0]
    assert x.shape[1] == theta.shape[0]

    return (
        np.sum(cost(hypothesis(theta, x), y)) + reg_param * np.sum(np.square(theta)) / 2
    ) / length


def gradient_descent_regularized(
    learning_rate: ParamType,
    reg_param: ParamType,
    theta: ParamVector,
    x: ParamMatrix,
    y: ParamVector,
    precision: ParamType = ParamType("0.0"),
) -> ParamVector:
    length = y.shape[0]
    assert length == x.shape[0]
    assert theta.shape[0] == x.shape[1]

    coefficient = learning_rate / length

    lambda_matrix = np.eye(theta.shape[0], dtype=ParamType)
    lambda_matrix[0, 0] = ParamType("0")
    lambda_matrix *= reg_param / length

    x_transposed = x.T
    while True:
        new_theta = (
            theta
            - coefficient * (x_transposed @ (hypothesis(theta=theta, x=x) - y))
            - (lambda_matrix @ theta)
        )
        yield new_theta

        if np.allclose(theta, new_theta, atol=precision, rtol=0):
            break

        theta = new_theta


assert np.allclose(
    np.repeat(1e10, 3),
    next(
        gradient_descent_regularized(
            1, 0, np.repeat(1e10, 3), np.ones((3, 3)), np.ones(3)
        )
    ),
)
