# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:sphinx
#     text_representation:
#       extension: .py
#       format_name: sphinx
#       format_version: '1.1'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

"""
# Лабораторная работа №9
"""

import re
from itertools import islice
from typing import Generator, NamedTuple

import numpy as np
from more_itertools import last
from nptyping import Array
from scipy.io import loadmat
from scipy.sparse.linalg import svds

ParamType = np.single

###############################################################################
# ### Загрузка данных

DATA = loadmat("ex9_movies.mat")
RATINGS = DATA["Y"].astype(ParamType)
IS_RATED = DATA["R"].astype(np.bool)

###############################################################################
# #### Типизация

MOVIE_COUNT, USER_COUNT = RATINGS.shape
FEATURE_COUNT = 30

FEATURES_SHAPE = (FEATURE_COUNT, MOVIE_COUNT)
Features = Array[ParamType, FEATURE_COUNT, MOVIE_COUNT]

OUTPUTS_SHAPE = (MOVIE_COUNT, USER_COUNT)
OutputsValidity = Array[np.bool, MOVIE_COUNT, USER_COUNT]
Outputs = Array[ParamType, MOVIE_COUNT, USER_COUNT]

WEIGHTS_SHAPE = (FEATURE_COUNT, USER_COUNT)
Weights = Array[ParamType, FEATURE_COUNT, USER_COUNT]


assert FEATURES_SHAPE[1] == OUTPUTS_SHAPE[0]
assert FEATURES_SHAPE[0] == WEIGHTS_SHAPE[0]

print(f"MOVIE_COUNT = {MOVIE_COUNT}")
print(f"USER_COUNT = {USER_COUNT}")
print(f"FEATURES_SHAPE = {FEATURES_SHAPE}")
print(f"OUTPUTS_SHAPE = {OUTPUTS_SHAPE}")

###############################################################################
# ## Функция стоимости
#
# \begin{equation}
# J(x^{(1)}, \ldots, x^{(n_m)}, \theta^{(1)}, \ldots, \theta^{(n_u)}) =
# \frac{1}{2} \sum_{(i,j):r(i,j)=1}
# \left(
# \left( \theta^{(j)} \right)^T x^{(i)} - y^{(i,j)}
# \right)^2
# + \frac{\lambda}{2} \sum_{i=1}^{n_m} \sum_{k=1}^{n}
# \left( x_k^{(i)} \right)^2
# + \frac{\lambda}{2} \sum_{j=1}^{n_u} \sum_{k=1}^{n}
# \left( \theta_k^{(j)} \right)^2
# \end{equation}


def compute_errors(
    theta: Weights, x: Features, r: OutputsValidity, y: Outputs
) -> Outputs:
    return r * ((theta.T @ x).T - y)


def loss(
    theta: Weights,
    x: Features,
    r: OutputsValidity,
    y: Outputs,
    reg_param: ParamType = 0,
):
    return (
        np.sum(np.square(compute_errors(theta, x, r, y)))
        + reg_param * (np.sum(np.square(x)) + np.sum(np.square(theta)))
    ) / 2


###############################################################################
# ### Инициализация значений
# $x^{(1)}, \ldots, x^{(n_m)}, \theta^{(i)}, \ldots, \theta^{(n_u)}$

INITIAL_FEATURES: Features = np.random.rand(*FEATURES_SHAPE)
INITIAL_WEIGHTS: Weights = np.random.rand(*WEIGHTS_SHAPE)


###############################################################################
# ## Градиентный спуск
def theta_gradient(
    theta: Weights,
    x: Features,
    errors: Outputs,
    learning_rate: ParamType,
    reg_param: ParamType = 0,
) -> Weights:
    return theta - learning_rate * ((errors.T @ x.T).T - reg_param * theta)


def x_gradient(
    theta: Weights,
    x: Features,
    errors: Outputs,
    learning_rate: ParamType,
    reg_param: ParamType = 0,
) -> Features:
    return x - learning_rate * ((errors @ theta.T).T - reg_param * x)


class GradientDescentOutput(NamedTuple):
    weights: Weights
    features: Features


def gradient_descent(
    theta: Weights,
    x: Features,
    r: OutputsValidity,
    y: Outputs,
    learning_rate: ParamType,
    reg_param: ParamType = 0,
    precision: ParamType = 0,
) -> Generator[Weights, None, None]:
    prev_loss = (
        loss(theta=theta, x=x, r=r, y=y, reg_param=reg_param) if precision else None
    )
    while True:
        errors = compute_errors(theta, x, r, y)
        new_theta = theta_gradient(theta, x, errors, learning_rate, reg_param)
        new_x = x_gradient(theta, x, errors, learning_rate, reg_param)
        yield GradientDescentOutput(new_theta, new_x)

        if precision:
            new_loss = loss(theta=new_theta, x=x, r=r, y=y, reg_param=reg_param)
            if (prev_loss - new_loss) < precision:
                break
            prev_loss = new_loss

        x = new_x
        theta = new_theta


###############################################################################
# ## Обучение модели
iterations, (theta, x) = last(
    islice(
        enumerate(
            gradient_descent(
                theta=INITIAL_WEIGHTS,
                x=INITIAL_FEATURES,
                r=IS_RATED,
                y=RATINGS,
                learning_rate=1e-4,
                reg_param=1,
                precision=1e-6,
            ),
            1,
        ),
        1000,
    )
)
print(f"{iterations} iterations")


###############################################################################
# #### Оценка результатов
initial_loss = loss(INITIAL_WEIGHTS, INITIAL_FEATURES, IS_RATED, RATINGS, 1)
final_loss = loss(theta, x, IS_RATED, RATINGS, 1)
assert final_loss <= initial_loss

print(f"initial loss = {initial_loss}")
print(f"final loss = {final_loss}")


###############################################################################
# #### Считывание названий фильмов

movie_name_regex = re.compile(r"(\d+\s+)([^\(]+)(?!\()")

with open("movie_ids.txt", encoding="latin1") as f:
    MOVIE_NAMES = [movie_name_regex.match(line).group(2) for line in f]


###############################################################################
# ### Добавление оценок

class Rating(NamedTuple):
    name: str
    id_: int
    mark: int


NEW_RATINGS = (
    Rating("Pulp Fiction", 55, 5),
    Rating("Trainspotting", 474, 5),
    Rating("Get Shorty", 3, 3),
    Rating("From Dusk Till Dawn", 16, 3),
    Rating("Taxi Driver", 22, 4),
    Rating("Godfather, The", 126, 5),
    Rating("Heat", 272, 5),
    Rating("Usual Suspects", 11, 2),
)

AUGMENTED_RATINGS = np.insert(RATINGS, USER_COUNT, values=0, axis=1)
AUGMENTED_IS_RATED = np.insert(IS_RATED, USER_COUNT, values=0, axis=1)
for rating in NEW_RATINGS:
    AUGMENTED_RATINGS[rating.id_, USER_COUNT] = rating.mark
    AUGMENTED_IS_RATED[rating.id_, USER_COUNT] = True

AUGMENTED_INITIAL_WEIGHTS = np.random.rand(FEATURE_COUNT, USER_COUNT + 1)

###############################################################################
# ### Обучение модели на обновлённых данных

iterations, (theta_aug, x_aug) = last(
    islice(
        enumerate(
            gradient_descent(
                theta=AUGMENTED_INITIAL_WEIGHTS,
                x=INITIAL_FEATURES,
                r=AUGMENTED_IS_RATED,
                y=AUGMENTED_RATINGS,
                learning_rate=1e-4,
                reg_param=1,
                precision=1e-6,
            ),
            1,
        ),
        1000,
    )
)
print(f"{iterations} iterations")


###############################################################################
# #### Извлечение рекомендаций

def get_n_largest_indices(a: np.ndarray, n: int) -> np.ndarray:
    partitioned_indices = np.argpartition(a, -n, axis=0)[-n:]
    return partitioned_indices[np.argsort(a[partitioned_indices])]


augmented_rating_hypothesis = theta_aug.T @ x_aug
for index in get_n_largest_indices(augmented_rating_hypothesis[:, USER_COUNT], 10):
    print(MOVIE_NAMES[index])

###############################################################################
# ### Обучение с помощью сингулярного разложения матрицы

user_features, weight_values, movie_features = svds(AUGMENTED_RATINGS, k=FEATURE_COUNT)
weight_matrix = np.diag(weight_values)
svd_rating_hypothesis = (user_features @ weight_matrix) @ movie_features

for index in get_n_largest_indices(svd_rating_hypothesis[:, USER_COUNT], 10):
    print(MOVIE_NAMES[index])
