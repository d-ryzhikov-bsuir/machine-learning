# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:sphinx
#     text_representation:
#       extension: .py
#       format_name: sphinx
#       format_version: '1.1'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

"""
# Лабораторная работа №5, часть 2
"""

import os
import re
from typing import NamedTuple, Pattern, Set

import numpy as np
from nltk.stem.snowball import SnowballStemmer
from selectolax.parser import HTMLParser
from scipy.io import loadmat
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import GridSearchCV, PredefinedSplit
from sklearn.svm import SVC

###############################################################################
# ### Загрузка данных
SPAM_TRAIN = loadmat("spamTrain.mat")
X_TRAIN = SPAM_TRAIN["X"]
Y_TRAIN = SPAM_TRAIN["y"].reshape(-1)

print(f"Train features shape: {X_TRAIN.shape}")
print(f"Train outputs shape: {Y_TRAIN.shape}")
print("=" * 69)

SPAM_TEST = loadmat("spamTest.mat")
X_TEST = SPAM_TEST["Xtest"]
Y_TEST = SPAM_TEST["ytest"].reshape(-1)

print(f"Test features shape: {X_TEST.shape}")
print(f"Test outputs shape: {Y_TEST.shape}")


###############################################################################
# ### Обучение классификатора и выбор оптимальных значений параметров $C$ и $\sigma^2$
test_fold = np.concatenate(
    (np.repeat(0, X_TRAIN.shape[0]), np.repeat(-1, X_TEST.shape[0]))
)
grid_search_clf = GridSearchCV(
    estimator=SVC(kernel="rbf"),
    param_grid={"C": np.arange(10, 20, 1), "gamma": np.arange(1e-3, 6e-3, 1e-3)},
    cv=PredefinedSplit(test_fold),
    n_jobs=-1,
).fit(np.concatenate((X_TRAIN, X_TEST)), np.concatenate((Y_TRAIN, Y_TEST)))

print(f"Best params: {grid_search_clf.best_params_}")
print(f"Best score: {grid_search_clf.best_score_:.2%}")


###############################################################################
# ### Предобработка текста письма
class Substitution(NamedTuple):
    pattern: Pattern
    repl: str

    def sub(self, s: str) -> str:
        return self.pattern.sub(self.repl, s)


SUBSTITUTIONS = (
    Substitution(re.compile(r"\bhttps?://\S+\b", re.IGNORECASE), " httpaddr "),
    Substitution(re.compile(r"\b\S+@\S+\b"), " emailaddr "),
    Substitution(re.compile(r"\d+"), " number "),
    Substitution(re.compile(r"\$"), " dollar "),
    Substitution(re.compile(r"[^\w\s\-]"), " "),
)


_word_regex = re.compile(r"\S+")
_stemmer = SnowballStemmer("english")


def preprocess_email(body: str) -> Set[str]:
    text = HTMLParser(body).text(strip=True, separator="\n")
    for s in SUBSTITUTIONS:
        text = s.sub(text)
    return {
        _stemmer.stem(match.group(0))
        for match in _word_regex.finditer(text)
    }


###############################################################################
# ### Загрузка словаря

VOCABULARY = {}
with open("vocab.txt") as f:
    for line in f:
        index, word = line.split()
        VOCABULARY[word] = int(index) - 1


###############################################################################
# ### Функция преобразования текста письма в вектор признаков

def encode_email(body: str):
    words = preprocess_email(body)
    word_indices = [VOCABULARY[w] for w in words if w in VOCABULARY]
    features = np.repeat(0, len(VOCABULARY))
    features[word_indices] = 1
    return features


###############################################################################
# ### Проверка работы классификатора на тестовых файлах
TEST_EMAIL_FILES = (
    "emailSample1.txt",
    "emailSample2.txt",
    "spamSample1.txt",
    "spamSample2.txt"
)

test_email_features = []
for test_file in TEST_EMAIL_FILES:
    with open(test_file) as f:
        content = f.read()
    test_email_features.append(encode_email(content))
test_email_features = np.array(test_email_features)

test_email_predictions = grid_search_clf.predict(test_email_features)
for name, is_spam in zip(TEST_EMAIL_FILES, test_email_predictions):
    print(f"{name} - {'' if is_spam else 'not '}spam")

###############################################################################
# ### Создание набора данных

SPAM_DIR = "spam"
SPAM_EMAILS = tuple(
    os.path.join(dirpath, fn)
    for dirpath, _, filenames in os.walk(SPAM_DIR)
    for fn in filenames
)[:10]

NON_SPAM_DIR = "non_spam"
NON_SPAM_EMAILS = tuple(
    os.path.join(dirpath, fn)
    for dirpath, _, filenames in os.walk(NON_SPAM_DIR)
    for fn in filenames
)[:10]
Y_CUSTOM = np.concatenate((np.repeat(1, len(SPAM_EMAILS)), np.repeat(0, len(NON_SPAM_EMAILS))))
count_vectorizer = CountVectorizer(input="filename", encoding="latin-1")

email_codes = count_vectorizer.fit_transform(
    SPAM_EMAILS + NON_SPAM_EMAILS,
    Y_CUSTOM
).toarray()

grid_search_clf_custom = GridSearchCV(
    estimator=SVC(kernel="rbf"),
    param_grid={"C": np.arange(10, 20, 1), "gamma": np.arange(1e-3, 6e-3, 1e-3)},
    n_jobs=-1,
).fit(email_codes, Y_CUSTOM)

print(f"Best params: {grid_search_clf_custom.best_params_}")
print(f"Best score: {grid_search_clf_custom.best_score_:.2%}")
